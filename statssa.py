from __future__ import absolute_import, print_function, division
# pylint: disable=W0622
from future.builtins import range, int, str
# pylint: enable=W0622

import pandas as pd
from datetime import date
import numpy as np

__author__ = 'William John Shipman'
__copyright__ = 'Copyright (c) 2015 William John Shipman'
__version__ = '0.1'
__date__ = '2015-12-25'
__updated__ = '2015-12-25'

def ascii_file_to_dataframe(fname, colname_fmt, index_tags):
    '''
    Load a text file provided by Statistics South Africa.
    
    Parameters
    ----------
    fname: str
        The path to the text data file.
    colname_fmt: str
        A format string using the tags H01 to H25 to specify how metadata in the Stats SA data should be used to format names for each time series.
    index_tags: list of str
        Each entry in the list is one of the tags H01 to H25 and specifieshow to build the hierarchical index for the columns of the returned DataFrame.
    Returns
    -------
    '''
    all_time_series = []
    with open(fname, 'r') as inputfile:
        for inputline in inputfile:
            if inputline[0] == 'H': # This is a meta-data field.
                tag, sep, value = inputline.partition(': ')
                if tag == 'H01':
                    all_time_series += [{'data':[], 'tags':{}}]
                all_time_series[-1]['tags'][tag] = value.strip()
            else:
                all_time_series[-1]['data'] += [float(inputline)]
                
    for tsdata in all_time_series:
        period_tag = tsdata['tags']['H25'].strip().lower()
        pandas_freq_from_period_tag = {'quarterly': 'Q', 'monthly': 'M'}[period_tag]
        _, start_year, start_month = tsdata['tags']['H24'].split(' ')
        index = pd.date_range(
            start=date(int(start_year), int(start_month), 1),
            freq=pandas_freq_from_period_tag,
            periods=len(tsdata['data']))
        tsdata['series'] = pd.Series(np.array(tsdata['data']),
                                     index=index,
                                     name=colname_fmt.format(**tsdata['tags']))
    df_data = {tuple(tsdata['tags'][tag] for tag in index_tags): tsdata['series'] for tsdata in all_time_series}
    df = pd.DataFrame(data=df_data)
    return df
